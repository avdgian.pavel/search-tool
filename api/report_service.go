package api

import (
	"context"
	"encoding/json"
	"fmt"

	"github.com/adjust/rmq/v4"
	"github.com/google/uuid"
	"gitlab.com/avdgian.pavel/search-tool/key"
	"gitlab.com/avdgian.pavel/search-tool/report"
	"gitlab.com/avdgian.pavel/search-tool/search"
)

type ReportService interface {
	CreateReport(ctx context.Context, sites []string, searchTerms []string) (*report.Report, error)
	GetReport(ctx context.Context, id report.ReportID) (*report.Report, error)
}

type reportService struct {
	repo  report.ReportRepository
	queue rmq.Queue
}

func NewReportService(repo report.ReportRepository, queue rmq.Queue) ReportService {
	return &reportService{
		repo,
		queue,
	}
}

func (s *reportService) CreateReport(ctx context.Context, sites []string, searchTerms []string) (*report.Report, error) {
	rep := report.NewReport(genReportID())
	for _, url := range sites {
		site := report.NewSiteReport(genSiteReportID(), url)
		rep.AddSite(site)
		for _, term := range searchTerms {
			st := report.NewSearchTermResult(genSearchTermResultID(), term)
			site.AddSearchTerm(st)
		}
	}

	err := s.repo.CreateReport(ctx, rep)
	if err != nil {
		return nil, fmt.Errorf("error saving report: %w", err)
	}

	task := search.NewSearchTask(rep.ID())
	taskBytes, err := json.Marshal(task)
	if err != nil {
		return nil, fmt.Errorf("error marshaling task: %w", err)
	}
	s.queue.PublishBytes(taskBytes)

	return rep, nil
}

func (s *reportService) GetReport(ctx context.Context, id report.ReportID) (*report.Report, error) {
	rep, err := s.repo.GetReport(ctx, id)
	if err != nil {
		return nil, fmt.Errorf("error fetching report: %w", err)
	}
	return rep, nil
}

type SearchKeyService interface {
	CreateSearchKey(apiKey string, searchEngineID string) (*key.SearchKey, error)
}

func genReportID() report.ReportID {
	return report.ReportID(uuid.NewString())
}

func genSiteReportID() report.SiteReportID {
	return report.SiteReportID(uuid.NewString())
}

func genSearchTermResultID() report.SearchTermResultID {
	return report.SearchTermResultID(uuid.NewString())
}
