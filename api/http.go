package api

import (
	"context"
	"encoding/json"
	"net/http"

	httptransport "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/mux"
)

func NewHTTPServer(
	ctx context.Context,
	endpts Endpoints,
) http.Handler {
	contentType := httptransport.SetContentType("application/json")
	options := []httptransport.ServerOption{
		httptransport.ServerAfter(contentType),
	}
	getRepOpts := []httptransport.ServerOption{
		httptransport.ServerBefore(func(ctx context.Context, req *http.Request) context.Context {
			return populateContextWithMuxVars(ctx, req)
		}),
	}
	getRepOpts = append(getRepOpts, options...)

	createReportTransport := httptransport.NewServer(
		endpts.CreateReport,
		decodeCreateReportRequest,
		encodeResponse,
		options...,
	)

	getReportTransport := httptransport.NewServer(
		endpts.GetReport,
		decodeEmptyRequest,
		encodeResponse,
		getRepOpts...,
	)

	r := mux.NewRouter()
	r.Methods("POST").Path("/api/reports").Handler(createReportTransport)
	r.Methods("GET").Path("/api/reports/{reportID}").Handler(getReportTransport)

	http.Handle("/", r)

	return r
}

func populateContextWithMuxVars(ctx context.Context, r *http.Request) context.Context {
	for k, v := range mux.Vars(r) {
		ctx = context.WithValue(ctx, ContextKey(k), v)
	}
	return ctx
}

func decodeEmptyRequest(_ context.Context, _ *http.Request) (interface{}, error) {
	return nil, nil
}

func decodeCreateReportRequest(_ context.Context, r *http.Request) (interface{}, error) {
	var request CreateReportRequest
	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		return nil, err
	}
	return request, nil
}

func encodeResponse(_ context.Context, w http.ResponseWriter, response interface{}) error {
	return json.NewEncoder(w).Encode(response)
}
