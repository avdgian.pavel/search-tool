package api

type Endpoints struct {
	ReportEndpoints
}

func NewEndpoints(reportService ReportService) Endpoints {
	return Endpoints{
		NewReportEndpoints(reportService),
	}
}
