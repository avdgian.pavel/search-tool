package api

import (
	"context"
	"errors"
	"time"

	"github.com/go-kit/kit/endpoint"
	"gitlab.com/avdgian.pavel/search-tool/report"
)

var ReportIdCtxKey ContextKey = "reportID"

type CreateReportRequest struct {
	Sites       []string `json:"sites"`
	SearchTerms []string `json:"searchTerms"`
}

type CreateReportResponse struct {
	ID        string               `json:"id"`
	CreatedAt string               `json:"createdAt"`
	State     int                  `json:"state"`
	Sites     []SiteReportResponse `json:"sites"`
}

type SiteReportResponse struct {
	URL         string               `json:"url"`
	State       int                  `json:"state"`
	SearchTerms []SearchTermResponse `json:"searchTerms"`
}

type SearchTermResponse struct {
	Term  string `json:"term"`
	Count int    `json:"count"`
	State int    `json:"state"`
}

type ReportEndpoints struct {
	CreateReport endpoint.Endpoint
	GetReport    endpoint.Endpoint
}

func NewReportEndpoints(service ReportService) ReportEndpoints {
	return ReportEndpoints{
		CreateReport: MakeCreateReportEndpoint(service),
		GetReport:    MakeGetReportEndpoint(service),
	}
}

func MakeCreateReportEndpoint(service ReportService) endpoint.Endpoint {
	return func(ctx context.Context, req interface{}) (interface{}, error) {
		request := req.(CreateReportRequest)
		rep, err := service.CreateReport(ctx, request.Sites, request.SearchTerms)
		if err != nil {
			return nil, err
		}
		response := reportToCreateResposne(rep)

		return response, nil
	}
}

func MakeGetReportEndpoint(service ReportService) endpoint.Endpoint {
	return func(ctx context.Context, req interface{}) (interface{}, error) {
		id, ok := ctx.Value(ReportIdCtxKey).(string)
		if !ok {
			return nil, errors.New("id is missing in context")
		}

		rep, err := service.GetReport(ctx, report.ReportID(id))
		if err != nil {
			return nil, err
		}
		response := reportToCreateResposne(rep)
		return response, nil
	}
}

func reportToCreateResposne(r *report.Report) CreateReportResponse {
	return CreateReportResponse{
		ID:        string(r.ID()),
		CreatedAt: r.CreatedAt().Format(time.RFC3339),
		State:     r.State(),
		Sites: func() []SiteReportResponse {
			sites := make([]SiteReportResponse, 0)
			for _, site := range r.Sites() {
				sites = append(sites, siteReportToResponse(site))
			}
			return sites
		}(),
	}
}

func siteReportToResponse(s *report.SiteReport) SiteReportResponse {
	return SiteReportResponse{
		URL:   s.URL(),
		State: s.State(),
		SearchTerms: func() []SearchTermResponse {
			terms := make([]SearchTermResponse, 0)
			for _, term := range s.SearchTerms() {
				terms = append(terms, searchTermResultToResponse(term))
			}
			return terms
		}(),
	}
}

func searchTermResultToResponse(st *report.SearchTermResult) SearchTermResponse {
	return SearchTermResponse{
		Term:  st.Term(),
		Count: st.Count(),
		State: st.State(),
	}
}
