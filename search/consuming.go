package search

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"sync"

	"github.com/adjust/rmq/v4"
	"github.com/pkg/errors"
	"gitlab.com/avdgian.pavel/search-tool/google"
	"gitlab.com/avdgian.pavel/search-tool/report"
)

const QueueName = "search-worker"
const Tag = "search-worker"

type SearchTask struct {
	ReportID string `json:"reportId"`
}

func NewSearchTask(id report.ReportID) SearchTask {
	return SearchTask{
		string(id),
	}
}

type SearchTaskConsumer struct {
	repo    report.ReportRepository
	sclient *google.SearchClient
}

func NewSearchTaskConsumer(repo report.ReportRepository, sclient *google.SearchClient) *SearchTaskConsumer {
	return &SearchTaskConsumer{
		repo,
		sclient,
	}
}

func (c *SearchTaskConsumer) Consume(delivery rmq.Delivery) {
	ctx := context.Background()

	var task SearchTask
	log.Printf("new task: %s\n", delivery.Payload())
	err := json.Unmarshal([]byte(delivery.Payload()), &task)
	if err != nil {
		reject(fmt.Errorf("error unmarshaling SearchTask: %w", err), delivery)
		return
	}
	rep, err := c.repo.GetReport(ctx, report.ReportID(task.ReportID))
	if err != nil {
		reject(fmt.Errorf("error looking up report %s: %w", task.ReportID, err), delivery)
		return
	}

	var wg sync.WaitGroup
	wg.Add(1)
	go func() {
		<-c.process(ctx, rep)
		wg.Done()
	}()
	wg.Wait()

	log.Printf("task %s: done\n", task.ReportID)
	ack(delivery)
}

type searchResult struct {
	searchTermID report.SearchTermResultID
	count        int
	err          error
}

func (c *SearchTaskConsumer) process(ctx context.Context, rep *report.Report) <-chan bool {
	searchChan := make(chan searchResult)
	defer close(searchChan)

	chans := make([]<-chan searchResult, 0)
	for _, site := range rep.Sites() {
		for _, searchTerm := range site.SearchTerms() {
			searchTerm.SetState(report.StateRunning)
			c.repo.UpdateReport(ctx, rep)
			chans = append(chans, c.doSearch(searchTerm.ID(), site.URL(), searchTerm.Term()))
		}
	}

	out := make(chan bool)
	go func() {
		for _, ch := range chans {
			log.Println("waiting for result...")
			res := <-ch
			log.Printf("received result: %s, %d. moving on", string(res.searchTermID), res.count)
			term, _ := rep.FindTermByID(res.searchTermID)
			if term != nil {
				term.SetCount(res.count)
				state := report.StateDone
				if res.err != nil {
					state = report.StateError
				}
				term.SetState(state)
				c.repo.UpdateReport(ctx, rep)
			}
		}
		log.Println("wrapping up")
		close(out)
	}()

	return out
}

func (c *SearchTaskConsumer) doSearch(id report.SearchTermResultID, url string, term string) <-chan searchResult {
	out := make(chan searchResult)

	go func() {
		log.Println("starting search...")
		count, err := c.sclient.CountOccurances(url, term)
		if err != nil {
			out <- searchResult{id, 0, err}
			return
		}

		out <- searchResult{id, count, nil}
		close(out)
		log.Println("search completed")
	}()

	return out
}

func ack(delivery rmq.Delivery) {
	if err := delivery.Ack(); err != nil {
		log.Fatal(fmt.Errorf("error acknowledging delivery: %w", err))
	}
}

func reject(err error, delivery rmq.Delivery) {
	log.Print(err)
	if err = delivery.Reject(); err != nil {
		log.Fatal(errors.WithStack(errors.New("error rejecting delivery")))
	}
}
