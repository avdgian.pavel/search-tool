package key

import "time"

type SearchKeyID string

type SearchKey struct {
	id             SearchKeyID
	apiKey         string
	searchEngineID string
	useCount       int
	lastUsedAt     time.Time
}

func NewSearchKey(id SearchKeyID, apiKey string, searchEngineID string) *SearchKey {
	return &SearchKey{
		id,
		apiKey,
		searchEngineID,
		0,
		time.Time{},
	}
}

func (s *SearchKey) ID() SearchKeyID {
	return s.id
}

func (s *SearchKey) APIKey() string {
	return s.apiKey
}

func (s *SearchKey) SearchEngineID() string {
	return s.searchEngineID
}

type SearchKeyRepository interface {
	CreateSearchKey(*SearchKey) error
	GetSearchKey(SearchKeyID) (*SearchKey, error)
	DeleteSearchKey(*SearchKey) error
}
