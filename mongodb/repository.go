package mongodb

import (
	"context"
	"fmt"
	"time"

	"gitlab.com/avdgian.pavel/search-tool/report"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

type reportDocument struct {
	ID        string               `bson:"_id"`
	CreatedAt time.Time            `bson:"created_at"`
	State     int                  `bson:"state"`
	Sites     []siteReportDocument `bson:"sites"`
}

type siteReportDocument struct {
	ID          string                     `bson:"_id"`
	URL         string                     `bson:"url"`
	State       int                        `bson:"state"`
	SearchTerms []searchTermResultDocument `bson:"search_terms"`
}

type searchTermResultDocument struct {
	ID    string `bson:"_id"`
	State int    `bson:"state"`
	Term  string `bson:"term"`
	Count int    `bson:"count"`
}

func documentToReport(doc reportDocument) *report.Report {
	rep := report.NewReportWithState(
		report.ReportID(doc.ID),
		doc.CreatedAt,
		doc.State)
	for _, st := range doc.Sites {
		rep.AddSite(documentToSiteReport(st))
	}
	return rep
}

func reportToDocument(rep *report.Report) reportDocument {
	sites := make([]siteReportDocument, len(rep.Sites()))
	for k, st := range rep.Sites() {
		sites[k] = siteReportToDocument(st)
	}
	return reportDocument{
		string(rep.ID()),
		rep.CreatedAt(),
		rep.State(),
		sites,
	}
}

func documentToSiteReport(doc siteReportDocument) *report.SiteReport {
	site := report.NewSiteReportFull(
		report.SiteReportID(doc.ID),
		doc.URL,
		doc.State)
	for _, docTerm := range doc.SearchTerms {
		site.AddSearchTerm(documentToSearchTerm(docTerm))
	}
	return site
}

func siteReportToDocument(site *report.SiteReport) siteReportDocument {
	searchTerms := make([]searchTermResultDocument, len(site.SearchTerms()))
	for k, term := range site.SearchTerms() {
		searchTerms[k] = searchTermToDocument(term)
	}
	return siteReportDocument{
		string(site.ID()),
		site.URL(),
		site.State(),
		searchTerms,
	}
}

func documentToSearchTerm(doc searchTermResultDocument) *report.SearchTermResult {
	return report.NewSearchTermResultFull(
		report.SearchTermResultID(doc.ID),
		nil,
		doc.Term,
		doc.Count,
		doc.State)
}

func searchTermToDocument(term *report.SearchTermResult) searchTermResultDocument {
	return searchTermResultDocument{
		string(term.ID()),
		term.State(),
		term.Term(),
		term.Count(),
	}
}

const collectionName = "reports"

type MongoReportRepository struct {
	db         *mongo.Database
	collection *mongo.Collection
}

func NewMongoReportRepository(db *mongo.Database) *MongoReportRepository {
	collection := db.Collection(collectionName)

	return &MongoReportRepository{
		db,
		collection,
	}
}

func (r *MongoReportRepository) GetReport(ctx context.Context, id report.ReportID) (*report.Report, error) {
	var repDoc reportDocument
	err := r.collection.FindOne(ctx, bson.M{"_id": string(id)}).Decode(&repDoc)
	if err != nil {
		return nil, fmt.Errorf("error retreiving document: %w", err)
	}
	return documentToReport(repDoc), nil
}

func (r *MongoReportRepository) CreateReport(ctx context.Context, rep *report.Report) error {
	repDoc := reportToDocument(rep)
	_, err := r.collection.InsertOne(ctx, repDoc)
	if err != nil {
		return fmt.Errorf("error inserting report document: %w", err)
	}
	return nil
}

func (r *MongoReportRepository) UpdateReport(ctx context.Context, rep *report.Report) error {
	doc := reportToDocument(rep)
	_, err := r.collection.ReplaceOne(ctx, bson.M{"_id": doc.ID}, doc)
	return err
}

func (r *MongoReportRepository) DeleteReport(ctx context.Context, rep *report.Report) error {
	_, err := r.collection.DeleteOne(ctx, bson.M{"_id": string(rep.ID())})
	return err
}
