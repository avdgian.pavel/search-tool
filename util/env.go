package util

import (
	"fmt"
	"os"

	"github.com/joho/godotenv"
)

type Config struct {
	MongoDSN string
	MongoDB  string
	RedisDSN string
}

func LoadEnv() (Config, error) {
	if err := godotenv.Load(".env"); err != nil {
		return Config{}, fmt.Errorf("error loading .env file: %w", err)
	}

	conf := Config{
		MongoDSN: os.Getenv("MONGO_DSN"),
		MongoDB:  os.Getenv("MONGO_DB"),
		RedisDSN: os.Getenv("REDIS_DSN"),
	}

	return conf, nil
}
