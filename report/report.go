package report

import (
	"context"
	"fmt"
	"time"
)

type ReportID string
type SiteReportID string
type SearchTermResultID string

const (
	StateNew = iota
	StateRunning
	StateDone
	StateError
)

type Report struct {
	id        ReportID
	createdAt time.Time
	state     int
	sites     []*SiteReport
}

func NewReport(id ReportID) *Report {
	return NewReportWithState(id, time.Now(), StateNew)
}

func NewReportWithState(id ReportID, createdAt time.Time, state int) *Report {
	// TODO: check state is a valid one
	sites := make([]*SiteReport, 0)
	return &Report{
		id,
		createdAt,
		state,
		sites,
	}
}

func (r *Report) ID() ReportID {
	return r.id
}

func (r *Report) CreatedAt() time.Time {
	return r.createdAt
}

func (r *Report) AddSite(site *SiteReport) {
	r.sites = append(r.sites, site)
	site.report = r
}

func (r *Report) NewSite(siteURL string) *SiteReport {
	site := &SiteReport{
		url: siteURL,
	}
	r.AddSite(site)
	return site
}

func (r *Report) Sites() []*SiteReport {
	return r.sites
}

func (r *Report) State() int {
	return r.state
}

func (r *Report) FindTermByID(id SearchTermResultID) (*SearchTermResult, error) {
	for _, s := range r.sites {
		for _, t := range s.searchTerms {
			if t.id == id {
				return t, nil
			}
		}
	}
	return nil, fmt.Errorf("no search result with the given id found")
}

func (r *Report) computeState() {
	r.state = computeParentState(r.state, r.sites)
}

type SiteReport struct {
	id          SiteReportID
	report      *Report
	url         string
	state       int
	searchTerms []*SearchTermResult
}

func NewSiteReport(id SiteReportID, url string) *SiteReport {
	state := StateNew
	searchTerms := make([]*SearchTermResult, 0)
	return &SiteReport{
		id,
		nil,
		url,
		state,
		searchTerms,
	}
}

func NewSiteReportFull(id SiteReportID, url string, state int) *SiteReport {
	searchTerms := make([]*SearchTermResult, 0)
	return &SiteReport{
		id,
		nil,
		url,
		state,
		searchTerms,
	}
}

func (s *SiteReport) ID() SiteReportID {
	return s.id
}

func (s *SiteReport) URL() string {
	return s.url
}

func (s *SiteReport) AddSearchTerm(term *SearchTermResult) {
	s.searchTerms = append(s.searchTerms, term)
	term.site = s
}

func (s *SiteReport) SearchTerms() []*SearchTermResult {
	return s.searchTerms
}

func (s *SiteReport) State() int {
	return s.state
}

func (s *SiteReport) computeState() {
	initialState := s.state
	s.state = computeParentState(initialState, s.searchTerms)
	if nil != s.report && initialState != s.state {
		s.report.computeState()
	}
}

type SearchTermResult struct {
	id    SearchTermResultID
	site  *SiteReport
	term  string
	count int
	state int
}

func NewSearchTermResultFull(id SearchTermResultID, site *SiteReport, term string, count int, state int) *SearchTermResult {
	return &SearchTermResult{
		id,
		site,
		term,
		count,
		state,
	}
}

func NewSearchTermResult(id SearchTermResultID, term string) *SearchTermResult {
	return NewSearchTermResultFull(id, nil, term, 0, StateNew)
}

func (t *SearchTermResult) ID() SearchTermResultID {
	return t.id
}

func (t *SearchTermResult) Term() string {
	return t.term
}

func (t *SearchTermResult) Count() int {
	return t.count
}

func (t *SearchTermResult) SetCount(count int) {
	t.count = count
}

func (t SearchTermResult) State() int {
	return t.state
}

func (t *SearchTermResult) SetState(state int) {
	t.state = state
	if nil != t.site {
		t.site.computeState()
	}
}

type ReportRepository interface {
	GetReport(context.Context, ReportID) (*Report, error)
	CreateReport(context.Context, *Report) error
	UpdateReport(context.Context, *Report) error
	DeleteReport(context.Context, *Report) error
}
