package report

type hasState interface {
	State() int
}

func computeParentState[S hasState](initialState int, children []S) int {
	newState := initialState
	allDone := true
	hasErrors := false
	for _, c := range children {
		if c.State() == StateRunning {
			newState = StateRunning
			allDone = false
			break
		}

		if c.State() == StateError {
			hasErrors = true
			allDone = false
		}
	}

	if allDone {
		newState = StateDone
	} else if hasErrors && newState != StateRunning {
		newState = StateError
	}

	return newState
}
