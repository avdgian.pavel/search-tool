package google

type StubSearchKeyProvider struct{}

func NewStubSearchKeyProvider() *StubSearchKeyProvider {
	return &StubSearchKeyProvider{}
}

func (p *StubSearchKeyProvider) GetAvailableKey() (GoogleSearchKey, error) {
	return GoogleSearchKey{
		apiKey: "AIzaSyBgljpALYYkjSzlrFLqb5wLDgela-6nDuk",
		cx:     "ad0a9b3408b0fb42d",
	}, nil
}
