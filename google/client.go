package google

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
)

const baseURL = "https://www.googleapis.com/customsearch/v1"

type GoogleSearchKey struct {
	apiKey string
	cx     string
}

func (sk GoogleSearchKey) GetAPIKey() string {
	return sk.apiKey
}

func (sk GoogleSearchKey) GetSearchEngineID() string {
	return sk.cx
}

type SearchKeyProvider interface {
	GetAvailableKey() (GoogleSearchKey, error)
}

type SearchResponseQuery struct {
	Title          string `json:"title"`
	TotalResults   string `json:"totalResults"`
	SearchTerms    string `json:"searchTerms"`
	Count          int    `json:"count"`
	StartIndex     int    `json:"startIndex"`
	InputEncoding  string `json:"inputEncoding"`
	OutputEncoding string `json:"outputEncoding"`
	Safe           string `json:"safe"`
	CX             string `json:"cx"`
	Filter         string `json:"filter"`
}

type SearchResponse struct {
	Kind              string                           `json:"kind"`
	URL               interface{}                      `json:"url"`
	Queries           map[string][]SearchResponseQuery `json:"queries"`
	Context           interface{}                      `json:"context"`
	SearchInformation interface{}                      `json:"searchInformation"`
	Items             interface{}                      `json:"items"`
}

type SearchClient struct {
	keyProvider SearchKeyProvider
}

func NewSearchClient(keyProvider SearchKeyProvider) *SearchClient {
	return &SearchClient{
		keyProvider,
	}
}

func (c *SearchClient) CountOccurances(site string, term string) (int, error) {
	res, err := c.makeRequest(site, term)
	if err != nil {
		return 0, fmt.Errorf("error making a request: %w", err)
	}
	count, err := strconv.Atoi(res.Queries["request"][0].TotalResults)
	if err != nil {
		count = 0
	}

	return count, nil
}

func (c *SearchClient) makeRequest(site string, term string) (target *SearchResponse, err error) {
	searchKey, err := c.keyProvider.GetAvailableKey()
	if err != nil {
		return nil, err
	}
	url := c.genURL(site, term, searchKey)

	resp, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	target = &SearchResponse{}
	err = json.NewDecoder(resp.Body).Decode(target)
	return
}

func (c *SearchClient) genURL(site string, term string, sk GoogleSearchKey) string {
	return fmt.Sprintf("%s?key=%s&cx=%s&q=site:%s+%s",
		baseURL, sk.GetAPIKey(), sk.GetSearchEngineID(),
		site, term)
}
