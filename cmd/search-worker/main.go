package main

import (
	"context"
	"log"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/adjust/rmq/v4"
	"gitlab.com/avdgian.pavel/search-tool/google"
	"gitlab.com/avdgian.pavel/search-tool/mongodb"
	"gitlab.com/avdgian.pavel/search-tool/search"
	"gitlab.com/avdgian.pavel/search-tool/util"
)

func main() {
	config, err := util.LoadEnv()
	if err != nil {
		log.Fatalln(err)
	}

	ctx := context.Background()

	errChan := make(chan error)
	connection, err := rmq.OpenConnection(search.Tag, "tcp", config.RedisDSN, 1, errChan)
	if err != nil {
		log.Fatal("failed to open redis connection: ", err)
	}

	taskQueue, err := connection.OpenQueue(search.QueueName)
	if err != nil {
		log.Fatal("error opening queue: ", err)
	}

	// Get Client, Context, CancelFunc and
	// err from connect method.
	mongoClient, ctx, cancel, err := mongodb.Connect(ctx, config.MongoDSN)
	if err != nil {
		panic(err)
	}

	// Release resource when the main
	// function is returned.
	defer mongodb.Close(mongoClient, ctx, cancel)

	// Ping mongoDB with Ping method
	err = mongodb.Ping(mongoClient, ctx)
	if err != nil {
		log.Fatal(err)
	}

	err = taskQueue.StartConsuming(10, time.Second)
	if err != nil {
		log.Fatal("consuming interupted by error: ", err)
	}

	db := mongoClient.Database(config.MongoDB)
	reportRepo := mongodb.NewMongoReportRepository(db)

	provider := google.NewStubSearchKeyProvider()
	gclient := google.NewSearchClient(provider)

	taskQueue.AddConsumer(search.Tag, search.NewSearchTaskConsumer(reportRepo, gclient))

	signals := make(chan os.Signal, 1)
	signal.Notify(signals, syscall.SIGINT)
	defer signal.Stop(signals)

	go func() {
		for {
			log.Print(<-errChan)
		}
	}()

	<-signals // wait for signal
	go func() {
		<-signals // hard exit on second signal (in case shutdown gets stuck)
		os.Exit(1)
	}()

	<-connection.StopAllConsuming()
}
