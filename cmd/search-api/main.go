package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"github.com/adjust/rmq/v4"
	"gitlab.com/avdgian.pavel/search-tool/api"
	"gitlab.com/avdgian.pavel/search-tool/mongodb"
	"gitlab.com/avdgian.pavel/search-tool/search"
	"gitlab.com/avdgian.pavel/search-tool/util"
)

func main() {
	config, err := util.LoadEnv()
	if err != nil {
		log.Fatalln(err)
	}

	ctx := context.Background()

	mongoClient, ctx, cancel, err := mongodb.Connect(ctx, config.MongoDSN)
	if err != nil {
		panic(err)
	}
	defer mongodb.Close(mongoClient, ctx, cancel)

	err = mongodb.Ping(mongoClient, ctx)
	if err != nil {
		log.Fatal(err)
	}

	db := mongoClient.Database(config.MongoDB)

	errChan := make(chan error)
	connection, err := rmq.OpenConnection(search.Tag, "tcp", config.RedisDSN, 1, errChan)
	if err != nil {
		log.Fatalln("failed to open redis connection: ", err)
	}

	taskQueue, err := connection.OpenQueue(search.QueueName)
	if err != nil {
		log.Fatalln("error opening queue: ", err)
	}

	reportRepo := mongodb.NewMongoReportRepository(db)
	reportService := api.NewReportService(reportRepo, taskQueue)

	endpts := api.NewEndpoints(reportService)

	go func() {
		c := make(chan os.Signal, 1)
		signal.Notify(c, syscall.SIGINT, syscall.SIGTERM)
		errChan <- fmt.Errorf("%s", <-c)
	}()

	go func() {
		log.Println("Starting up http server...")
		r := api.NewHTTPServer(ctx, endpts)
		errChan <- http.ListenAndServe("127.0.0.1:8080", r)
	}()

	log.Fatalln(<-errChan)

	// provider := google.NewStubSearchKeyProvider()
	// gclient := google.NewSearchClient(provider)

	// count, err := gclient.CountOccurances("softblues.io", "vue.js")
	// if err != nil {
	// 	log.Fatal(err)
	// }

	// fmt.Printf("Result count: %d\n", count)
}
