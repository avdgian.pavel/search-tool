.PHONY: mongo search-api search-worker redis

mongo:
	docker run --rm -ti -v $(shell pwd)/.data:/data/db \
		--name searchtool-mongo \
		-p 127.0.0.1:27017:27017/tcp \
		-e MONGO_INITDB_ROOT_USERNAME=root \
		-e MONGO_INITDB_ROOT_PASSWORD=o6aYZ5425xPZnTzVFyQa \
		mongo:5.0.8

redis:
	docker run --rm -ti -v $(shell pwd)/.redisdata:/data/redis \
		--name searchtool-redis \
		-p 127.0.0.1:6379:6379/tcp \
		redis:7.0-alpine

search-api:
	go run cmd/search-api/main.go

search-worker:
	go run cmd/search-worker/main.go
